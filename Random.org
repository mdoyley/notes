* Random
  :PROPERTIES:
  :ID:       7718297B-EDE4-443D-A5E4-905AC3F7E239
  :END:
  [2015-10-24 Sat 14:17]

I am feeling uneasy and I am not sure why. Mbaybe its because I am still trying come up with a more efficient
way of working. Its amazing, its 15 y since I completed my PhD and I am still struggling with this. Maybe I
know what to do, but I have chose to ignore what I have learned. One first glance what have I learned.

- Plan a day in advance
- Write everything down
- Use an outline when writing
- Right fast
- Review Weekly
- Focus on the important tasks in the morning
- Incorporate technology in workflow
- Focus on the big ticket items
- Make time for prayer and meditation
- Learn to write better
- Be a more efficient reader
- Strive to reach my ideal day
- Make use of dead time
- Work on one task at a time
- Delegate
- Don't let email rule me
- Give everything a place
