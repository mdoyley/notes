:PROPERTIES:
:ID:       563E6FC3-0181-4CF4-9676-2A5CF1023213
:END:
#+TITLE: Bone healing
#+startup:inlineimages
#+startup: showall
- tags: #ARF, #App
Healing [[id:26855A00-24A0-43C2-AF3E-8FC9327618D8][§Bone defects]] involes bone formation caused by mesenchyme stem cells in the trabecular bone
marrow. But this is very consuming because decreased blood supply. Several studies have demonstrated
that LIPUS promotes stem cell differentiation and enhances blood supply, recruit bone marrow
cells into the healing region. The most critical time for bone healing is the 1st 1 to 2 w after
injury, during which inflammation and re-vascularization occurs. 

cite:liu18_accel_bone_defec_healin_regen studied the effect of low intensity pulsed ultrasound
(LIPUS) induced radiation on trabecular bone defect repair and healing.  To investigate this they
created a 3.5m diameter defect in the proximal bilateral tibia region of rats and applied LIPUS to
the left tibia for 20 m,in everyday for 2 wks. Using Histomorpholmetry and Micro-CT the observed
LIPUS exhibited higher bone density, connectivity density, trabecular number, bone mineral density,
lower trabecular groups. Mechanical testing revealed increased bone stiffness but no increase in the
ultimate load at the mid-shaft of the tibia.

-----
- Related::
  - [[id:68AB51D8-4F16-434D-A66D-45BB59590E06][§Acoustic Radiation force]]
  - [[id:AC172DCB-21A2-4C9D-8F91-31E44A8E078D][§Mechanical testing of bone]]

