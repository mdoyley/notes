:PROPERTIES:
:ID:       5EE56C0B-AC9E-456D-B4D5-14D6C447D63D
:END:
#+title: How to be a prolific writer
- tags:: writing

To survive in academic environment, one has to be a prolific writer, which means writing
manuscripts, grants fast. This is new skill that I am teaching myself.

Like most things, this is not rocket science, but rather a change in behavior.

1) Writing has to be a major priority in my life. Prolific writers write every day. This time is
   precious guard them [min 3 hrs/day]

2) Write the first draft quickly - one setting. Use coding or notes were info is lacking

3) Set a deadline for each writing project - eliminates perfectionism

4) Use first draft to structure document, I am a firm believer that the goal of the first draft tis
   to figure out what is in your mind, and with subsequent revision you revise or restructure it to
   tell a consistent story.

5) It is easy to write when there is something to work with, so writing should be a part of our
   thinking and research process. As ideas comes write them down. Write brief summaries when
   researching a topic or reading a paper - materials I can use in later.

6) Get an editor to final polishing.

2009-03-07 11:26:59
---
