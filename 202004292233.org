:PROPERTIES:
:ID:       390DDDB7-C806-4225-9784-C2C935783E61
:END:
#+TITLE: Microchannel flow model
#+Author: Marvin M. Doyley
#+options: toc:nil
#+startup:inlineimages
#+startup: showall
tags::
-----
cite:Parker_2014 describe an novel theological model that treats soft tissues as a network of
micro-channels containing viscous fluids. The beauty of this model is that captures both the time
domain (stress-relaxation) and frequency domain (dispersion) behavior of soft tissues.  
\sigma_{i}(t)=\mathrm{A}_{0} \Gamma[a]\left[-a / t^{(a+1)}\right] \text { for } t>0
and in the frequency domain

\boldsymbol{E}(\omega)=\frac{A_{0}}{\sqrt{2 \pi}} \Gamma[a] \Gamma[1-a] \mathrm{Abs}[\omega]^{a}\left(\cos \left[\frac{a \pi}{2}\right]+\mathrm{j} \operatorname{sign}[\omega] \sin \left[\frac{a \pi}{2}\right]\right)
Where $E(\omega) is tissue strain transfer function in time domain. The main idea is that if tissue
has a distribution of micro-channels given by a power law relaxation spectrum (A(\tau)=\tau^{-b})
the stress relaxation will show a \sigma_{S R} \cong t^{1-b}=1 / t^{a} response, and the
tissue-strain transfer function in the frequency domain, E(\omega) will have
an |\omega|^{b-1}=|\omega|^{a} dependence 

The simplest model only uses two parameters to model the stress relaxation; however, they also
described more complex models
[[./Micro-channel.png]]
The 3 parameter version of the model looks like the KVFD model.The main contribution of this work is
that it accounts for different size micro-channels that is not accounted for in any of the previous
rheological models. Although this is a break through to my knowledge no work have been done to
confirm who well the model does this or to evaluate how increasing the complexity of the model
improves the goodness of fit to stress relaxation data obtained from different tissues. This is one
of those papers that I will need to re-read to appreciate it.

-----

