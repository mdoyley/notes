:PROPERTIES:
:ID:       B7C2AAF2-D64A-4ACF-810D-4245D33E8281
:END:
#+TITLE: Gaussian forcing function
#+Author: Marvin M. Doyley
#+options: toc:nil
#+startup:inlineimages
#+startup: showall
tags::
-----
The symmetric case: \vec{f}(r, t)=F_{0} e^{-r^{2} / \sigma^{2}} W(t) \hat{z} 
The Asymmetric case: \vec{f}(x, y, t)=F_{0} e^{-x^{2} / \sigma_{x}^{2}} e^{-y^{2} /
\sigma_{y}^{2}}W(t) \hat{z}


where \frac{\sigma_{x}}{\sigma_{y}} is the aspect ratio of the Gaussian excitation.


-----
[[id:8E301CFA-3515-4006-A604-6DAAB3ECC72F][§Fourier domain description of shear waves]]
