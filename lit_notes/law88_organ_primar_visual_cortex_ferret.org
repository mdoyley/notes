:PROPERTIES:
:ID:       CA000642-894A-4B11-A468-1DE8E6D605ED
:ROAM_REFS: cite:law88_organ_primar_visual_cortex_ferret
:END:
#+TITLE: law88_organ_primar_visual_cortex_ferret: Organization of primary visual cortex (area 17) in the ferret
- tags :: functional ultrasound
- Used Anatomical and electrophysioligcal mapping to determine how the occular primary dominance
  columns are arranged in the visual cortex
- Primary visual cortex (referred to as area 17)-region within the brain responsible for vision in
  the ferret
- Mapped size and distribution of Area 17 by measuring the response of groups of neurons to visual stimulus
- Photograph of the exposed brain noting the location of electrode penetration with blood vessel
 land marks
-  EEG recording from singe and small clusters of neurons 

