:PROPERTIES:
:ID:       98161311-A0B5-4BA2-900C-20D83A0D6FF0
:END:
#+title:Where does time go

I always find it strange that I come to the end of the day wondering what have I done all day. Today
was one of those days that slip through my fingers. How can I prevent this from happening tomorrow?
before looking at different options to dealing with this, one question that I should ak myself is
this a issue of burn out? I definitely lost my enthusiasm that feels like a big weight around my
neck. In July when I started this job I was full of enthusiasm. So one of the think that I need to
think about is how to pace myself. this is a four race, a marathon and I need to pace my self to
finish the race strong. The issue is this administrative position: one where I am full faculty and
full administrator. But maybe that is the problem, I am not full-time administrator but rather a
part-time one and I should remember that. Manage myself more or better. Thinking and writing is the
new thing that should enhance my efficiency or allow me to write more and think better as well. so
this is also a question about how to divide myself among my roles. Fulfilling all my roles without
burning up that is the real question? After ABET I should sit and think about what I have learn
this semester.

Using the morning to think or meditate is something that I should do. This should be part of
morning ritual, grounding myself before embarking on the day.  Maybe that is it? I need to learn to
settle myself an think about a given task picture what I want to accomplish getting things firmed in
my head before doing so. maybe this is what Jesus was doing when he was praying in early morning,
getting a picture of his mission clarifying this in his mind and conversion with God. Seeing the
course of the day in one minds eyes. 

The key to answering this question is figuring what is causing time to
slip away and figuring out how to stop the time killers. I always feel time is like money, you have
to a play for every dollar-- as Chalkar would say money is your soldier and you need to put them to
work. So in essence, what I am saying is that I have to start my day with a plan. Be deliberate or
time will slip away is the lesson here. This is nothing new, I know this but for some reason I
cannot follow this simple principle. I need to discipline myself to plan the night ahead.  So
*planning* is first step, the next step is execution. More specifically, doing the things that I plan
the night before. Minimizing distractions, and staying true to my plans. Since it is difficult to
know ahead of time the disruption that could happen, I need to also building a couple of hours to
account for things that I didn't plan for. In essence I must be flexible about planning to account
for the unplanned: To summarize the three step plan that I propose to Implement is as follows
1. Plan the night before
2. Minimizing distractions
3. Account for interruptions

