:PROPERTIES:
:ID:       E6389775-E1CB-4AE3-89F2-F19874B4B8CB
:END:
#+title: Note taking as a way of life

I now realize is the way I capture notes is not very effective. I typical jot down notes in the
margin of the book when I am reading. Although this practice helps to keep my focus and
concentration while reading, engage the material, after that I do nothing. Those snippets of
knowledge, insight, question, or idea just stays in the margin--I never see them again unless I
reread the book, what a waste. I realize this wean I was reviewing the Swedish PhD thesis. Little
nuggets of information sprinkled through out the thesis. Typically, I would not these down scribble
my thoughts in the margin never to come back to them. Losing what I have learned. So what I will do
in the future is to process all my little thoughts things that I find interesting after every
reading exercise. So what will this look like? I guess this will depend on the material that I am
reading.
- books: process either after each chapter or after reading the entire book. I will need to
  experiment with this a little to see which approach is better.
- Articles: After reading or I could batch several articles in one sitting
- others: Since I spend so much time listen audio books, it would be nice to figure this out.
