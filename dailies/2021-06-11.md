:PROPERTIES:
:ID:       49A49344-9A6A-4260-B030-FBD4E56C6DFB
:END:
#+STARTUP: showeverything
#+title: 2021-06-11
There are a lot of things that I need finish before I leave for my vacation in July.
ABET Self study report:
***** DONE Review paper for PMB
***** TODO Review Swedish student thesis 
***** TODO Write ECE's research strategic plan
I must confess that this feels overwhelming. So the question is how do I cope with this and how do I
prevent stuff from pilling up in the future?  One idea is to focus less on the minor stuffs.
Tweaking of Emacs configuration is truly a waste time. I also need to improve my writing skills. It
takes me far too long to produce stuff. So I need to teach myself how to think and write faster.
