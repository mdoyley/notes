:PROPERTIES:
:ID:       73C72727-22D6-4A43-B3A1-44C02C019DB1
:END:
#+title:Writing fast

I am teaching myself to get ideas down fast on paper because this is the key to good writing.  I
need to learn how type both fast and accurate.  This will allow me to implement the writing idea
that I read several years ago: write for 45 m nonstop, group ideas after, map a cluster map, and
rewrite the piece based on the outline. This is kind of a reverse outline.  The rational is to
repeat this process until I am happy with the material.  I also need to learn how to complete a
section in one sitting.  This should improve my focus and teach me how to writing in small
burst. The last thing I need to learn is typing without using the backspace key--akin to writing
with out using an eraser.  To summarize here are the things that I want to do
1. Type fast and accurate
2. Write fast drafts really fast and use this as a way to brainstorm and cluster idea
3. Refrain from using the backspace too often. I am looking into how to disabling this on my
   computer but I am still haven't figured out how to do this.
4. Complete major section in one sitting. This is an idea that Brian Pogue told me several years
   ago but I have not manage to implement in my workflow.

