:PROPERTIES:
:ID:       E7CFA688-F5DB-4D89-9C2B-EE284569556B
:END:
#+TITLE: hypoxic-ischemic brain damage
#+options: toc:nil
#+startup:inlineimages
- tags:: 
Cardiac arrest or profound hypotension can cause hypoxic-ischaemic brain injury, which is the
leading cause of mortality and long-term neurologic disability in survivors cite:sekhon17_clinic_pathop_hypox_ischem_brain

---
- Related::

