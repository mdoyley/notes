:PROPERTIES:
:ID:       E8B5136E-6E9A-4C49-BB4C-F49E8AFABC6E
:END:
#+title: Elastography assessment of ulcerative colitis
-tags:: #ARF, #App

Ultrasound can detect inflammation (bowel wall thickening, increased blood flowing within the
thickened wall) in ulcerative [[https://www.gwhospital.com/conditions-services/digestive-disorder-center/colitis][colitis]] patients. /The hypothesis is that inflammation changes bowel
wall stiffness./ cite:goertz19_acous_radiat_force_impul_arfi investigated the feasibility of using
[[id:BDA73856-6289-4152-BDB8-A5520FF0F3C8][acoustic radiation force imaging]] as a compliment to high-frequency transabdominal imaging when
assessing the extent of inflammatory bowel disease.The severity of inflammation assessed with the
Mayo-subscore (rectal-bleeding (0-3 points), subjective appraisal of disease activity by physician
(0-3 points), stool frequency over three days (0-3 points)). 0-1 points clinical remission, 2-4
points mild disease, 5-7 points moderate, and >7 points severe activity. 13 healthy subjects and 20
patients with UC. Evaluated SWS in different bowel segments (Terminal ileum, Ascending colon,
Transverse colon, Descending colon, Sigmod colon)

 [[id:E00CB26F-034A-442D-9CAF-A2DAC5FCEA80][Shear wave speed]] was evaluated with a commercially available ARFI technique (Virtual Touch tissue
quantification, Siemens)
1. SWS of the Sigmoid and Transverse colon bowel segments of UC patients were higher than the
   controls. No difference in SWS was observed for the ascending and descending colon
2. Bowel think of UC patients were higher than those of the control group

Demonstrated the feasibility of ARFI of the bowel wall of the colonic frame and terminal ileum, but
showed high standard deviation.


