* Note-Taking
  :PROPERTIES:
  :ID:       5CD06245-115C-4539-A3AD-BDA4B3D1B877
  :END:
Every year I reflect on my note-taking practices. This generally involve searching the Internet for different
views on the topic. However, this year I've decided to devise a method of my own  laying the framework for
building and improving this skill. Many overlook this topic, but it is a key ingredient to academic
success. There are some common elements to all successful note-taking strategies, but the key components for
me are as follows:
1. Conciseness short sentences of phrases.
2. Summarize the key ideas.
3. Personal contain  shorthands or drawings.
4. Active include questions, action-items, connections, or ideas.

There have been many discussions on digital versus analog note-taking strategies, but it doesn't
matter which format you employ. Sure there are advantages and disadvantages associated with each
approach, but it is a personal choice. As a lifelong learner I could argue that revision is an
import component of any note-taking system. Why spend hundreds of hours making detailed notes if
your are not going to refer to them. When you review your notes how this discipline provide fresh
perspective on a topic, generate new questions and ideas, highlight the gaps in your current
knowledge.

In terms of specifics, I recommend using a two-step notetaking strategy:
1. Take rough notes as you read or listen to a lecture or meeting
2. Revise my rough notes, and make a more concise version.

The key advantage of this system is that it allows one to use a hybrid approach. Specifically for
the first step one could use an analog format, and then employ digital format for the last version
the best of both worlds. As you revise the note please take the key components highlighted in the
previous paragraph.
