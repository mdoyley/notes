:PROPERTIES:
:ID:       A2410219-D1D2-4049-9794-890CB6AB1965
:END:
#+TITLE: Lamb waves
#+options: toc:nil
#+startup:inlineimages
#+startup: showall

Lamb waves are a type of guided wave generated in plates and shells.

-----

