:PROPERTIES:
:ID:       BDA73856-6289-4152-BDB8-A5520FF0F3C8
:END:
#+TITLE: Acoustic radiation force imaging
#+startup:inlineimages
#+startup: showall
tags:: #shearWave, #ARF
-----
Acoustic radiation force imaging looks at tissue dynamics in response to radiation force. These
dynamics properties include: (1) time-to-peak displacements, .  
- What is the theoretical basis for ARFI.
- The role of directional filtering in ARFI 

-----
- Related::
  - [[id:7E11A9B3-ED48-4034-9C47-C3E0241DD04A][§Group vs phase velocity]]
  - [[id:2F1F97D8-96B4-475D-841A-2A9205ECD326][§Shear wave dispersion ultrasound vibrometry]]
  - [[id:E1FCACB4-5871-4358-8D7D-F312830C642B][§Transient elastography]]
  - 

