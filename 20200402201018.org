:PROPERTIES:
:ID:       EB2494FA-3B06-4D5D-937C-87FE3BCFE403
:END:
#+TITLE: Liver elastography
#+Author: Marvin M. Doyley
#+options: toc:nil
#+startup:inlineimages
#+startup: showall
tags::
-----
 Liver is perhaps one of the most successful application of shear wave elastography.  Quasi-static
 elastography cannot assess liver stiffness because it is difficult to perform between the
 intercostal muscle. Fibroscan and MRE is the most widely used approach for assessing liver
 stiffness, information that is used to assess the degree of fibrosis one of the early signs of
 liver damage. The most popular scale for liver fibrosis stagging is the METAVIR scoring system. F0
 (no fibrosis), F1 (portal fibrosis without septa), F2 (portal fibrosis with few septa), F3
 (numerous septa with out cirrhosis), and F4 (cirrhosis). Inflammation or injury are the main causes
 of fibrosis. Several studies have compared the degree of liver stiffness and the degree of fibrosis
 as determined by biopsy with different shear wave elastography approach. In a perspective study
 with 215 patients (hepatitis C or B) it was demonstrated that 2D SWE was more reliable than
 fibroscan. More specifically, it failed only 12 cases where as fibroscan failed in 30 cases
 cite:Osman_2020. Nevertheless there was good agreement between shear wave speed estimated with
 fibroscan and 2D SWE (86.7%.); however SWE overestimates the degree of fibrosis. SWE efficacy was
 highest when applied to patients with F0, F1, and F4 cases and performed better with obese or
 patients with ascites. Barr stated in a recent reviewed article cite:Barr_2017 that the main reason
 for using elastography to assess liver fibrosis is that it could potentially be used to identify
 patients with compensated cirrhosis earlier to prevent progression of the disease. They also stated
 that 2D-SWE have higher diagnostic accuracy than fibroscan for diagnosing sever Fibrosis (\ge
 F3). They also stated that different manufactures have reported different SWS cut-off values for
 stagging fibrosis.
 
 [[./Metavir.png]]

----
[[id:89F345F2-01CB-48E7-B295-5EC343709903][§Linear mixed models and statistical significance]]


