:PROPERTIES:
:ID:       FD38B7F2-A430-490C-9C15-8E92CAB6746D
:END:
#+title: Deleting .svn files reclusively 
find . -type d -name '.svn' -print -exec rm -rf {} \;
