:PROPERTIES:
:ID:       89F345F2-01CB-48E7-B295-5EC343709903
:END:
#+TITLE: Linear mixed models and statistical significance
#+Author: Marvin M. Doyley
#+options: toc:nil
#+startup:inlineimages
#+startup: showall

-----
A linear mixed model can determine if changes in shear modulus is indicative of disease. Hines and
colleagues cite:hines10_repeat_magnet_reson_elast_quant_hepat_stiff used this statistical model to
determine the repeatability of magnetic resonance elastography. Specifically, the used to determine
how the primary variables (a) biological, (b) operator, etc. impacts shear modulus. Doing this
allowed them to determine if the observed changes in shear modulus represents differences between
normal and diseased tissues rather than statistical fluctuations. *Fig4* is a nice plot. Using this
statistical model they determined that the differences between the test and control group is
significant if it is greater than 37%.  
-----
links::
